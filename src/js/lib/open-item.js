$(function() {
	var openItemSlider = $('.js-open-item-slider'),
		accordion = $('.js-accordion'),
		defaultOptions = {
			variableWidth: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			cssEase: 'ease-in-out',
			useTransform: true,
			speed: 750,
			infinite: false,
			prevArrow: '<button type="button" class="page-open-item__prev"><svg class="icon icon-arrow-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-left"></use></svg></button>',
			nextArrow: '<button type="button" class="page-open-item__next"><svg class="icon icon-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-right"></use></svg></button>',
			responsive: [
				{
					breakpoint: 1279,
					settings: {
						slidesToShow: 4,
						prevArrow: '<button type="button" class="page-open-item__prev"><svg class="icon icon-arrow-slider"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-slider"></use></svg></button>',
						nextArrow: '<button type="button" class="page-open-item__next"><svg class="icon icon-arrow-slider"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-slider"></use></svg></button>'
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
						prevArrow: '<button type="button" class="page-open-item__prev"><svg class="icon icon-arrow-slider"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-slider"></use></svg></button>',
						nextArrow: '<button type="button" class="page-open-item__next"><svg class="icon icon-arrow-slider"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-slider"></use></svg></button>'
					}
				}
			]
		};

	openItemSlider.slick($.extend(true, defaultOptions, {}));

	//Accordion
	accordion.each(function () {
		var _this = $(this),
			_thisItem = _this.children(),
			_thisArrow = _this.find('.arrow-bot-anim'),
			_thisTitle = _this.find('.accordion__title'),
			_thisText = _this.find('.accordion__text');

		_thisTitle.on('click', function () {
			if ($(this).closest(_thisItem).hasClass('is-active')) {
				$(this).find(_thisArrow).removeClass('is-active');
				$(this).next().slideUp().closest(_thisItem).removeClass('is-active');
			} else {
				$(this).closest(_thisItem).siblings().filter('.is-active').each(function () {
					$(this).removeClass('is-active').find(_thisText).slideUp();
					$(this).find(_thisArrow).removeClass('is-active');
				});
				$(this).find(_thisArrow).addClass('is-active');
				$(this).next().slideDown().closest(_thisItem).addClass('is-active');
			}
		});
	});

});