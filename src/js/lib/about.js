$(function () {
	var partnersSlider = $('.js-partners-slider'),
		whyAboutSlider = $('.js-why-about-slider'),
		clinicSlider = $('.js-clinic-slider'),
		historySliderNav = $('.js-history-nav'),
		historySliderFor = $('.js-history-for'),
		historyLineMob = $('.js-history-line-mob'),
		historyLineMobHeight = historyLineMob.height(),
		windowWidth = $(window).width();

	historySliderNav.on('beforeChange', function(event, slick, currentSlide, nextSlide){
		TweenMax.to(historyLineMob, 0.25, {
			height: 0,
			ease: Power1.easeOut,
			onComplete: function () {
				TweenMax.to($(this.target), 0.5, {
					height: historyLineMobHeight,
					delay: 0.5,
					ease: Power1.easeIn
				});
			}
		});
	});

	function initHistorySlider() {
		historySliderFor.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			fade: false,
			cssEase: 'ease-in-out',
			speed: 750,
			infinite: false,
			useTransform: true,
			asNavFor: historySliderNav,
			prevArrow: '<button type="button" class="history__prev"><svg class="icon icon-arrow-slider"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-slider"></use></svg></button>',
			nextArrow: '<button type="button" class="history__next"><svg class="icon icon-arrow-slider"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-slider"></use></svg></button>'
		});
		historySliderNav.slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: historySliderFor,
			useTransform: true,
			dots: false,
			cssEase: 'ease-in-out',
			speed: 750,
			arrows: false,
			centerMode: true,
			infinite: false,
			centerPadding: 0,
			focusOnSelect: true
		});
	}

	function toggleHistorySlider(breakpoint) {
		if ($(window).width() < breakpoint) {
			if (!historySliderNav.hasClass('slick-initialized')) {
				initHistorySlider();
			}
		} else {
			if (historySliderNav.hasClass('slick-initialized')) {
				historySliderNav.slick('unslick');
				historySliderFor.slick('unslick');
			}
		}
	}

	toggleHistorySlider(768);
	toggleClinicSlider(768, clinicSlider);
	togglePartnersSlider(1220, 768, partnersSlider);
	toggleWhyAboutSlider(1220, whyAboutSlider);
	$(window).resize(function () {
		if ($(window).width() != windowWidth) {
			toggleHistorySlider(768);
			toggleClinicSlider(768, clinicSlider);
			togglePartnersSlider(1220, 768, partnersSlider);
			toggleWhyAboutSlider(1220, whyAboutSlider);

			windowWidth == $(window).width();
		}
	});

	function togglePartnersSlider(breakpoint, breakpointMobile, slider) {
		if ($(window).width() < breakpoint && $(window).width() >= breakpointMobile) {
			if (!slider.hasClass('slick-initialized')) {
				initPartnersSlider(slider);
			} else {
				slider.slick('unslick');
				initPartnersSlider(slider);
			}
		} else if ($(window).width() >= breakpoint) {
			if (slider.hasClass('slick-initialized')) {
				slider.slick('unslick');
			}
		} else {
			if (!slider.hasClass('slick-initialized')) {
				initPartnersSliderRows(slider);
			} else {
				slider.slick('unslick');
				initPartnersSliderRows(slider);
			}
		}
	}

	function toggleClinicSlider(breakpoint, slider) {
		if ($(window).width() < breakpoint) {
			if (!slider.hasClass('slick-initialized')) {
				initClinicSlider(slider);
			}
		} else {
			if (slider.hasClass('slick-initialized')) {
				slider.slick('unslick');
			}
		}
	}

	function toggleWhyAboutSlider(breakpoint, slider) {
		if ($(window).width() < breakpoint) {
			if (!slider.hasClass('slick-initialized')) {
				initWhyAboutSlider(slider);
			}
		} else {
			if (slider.hasClass('slick-initialized')) {
				slider.slick('unslick');
			}
		}
	}

	function initClinicSlider(slider) {
		slider.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: true,
			cssEase: 'ease-in-out',
			useTransform: true,
			speed: 750,
			rows: 6,
			prevArrow: '<button type="button" class="clinic__prev"><svg class="icon icon-arrow-slider"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-slider"></use></svg></button>',
			nextArrow: '<button type="button" class="clinic__next"><svg class="icon icon-arrow-slider"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-slider"></use></svg></button>'
		});
	}

	function initPartnersSlider(slider) {
		slider.slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			infinite: true,
			cssEase: 'ease-in-out',
			useTransform: true,
			speed: 750,
			rows: 3,
			prevArrow: '<button type="button" class="partners__prev"><svg class="icon icon-arrow-slider"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-slider"></use></svg></button>',
			nextArrow: '<button type="button" class="partners__next"><svg class="icon icon-arrow-slider"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-slider"></use></svg></button>'
		});
	}

	function initPartnersSliderRows(slider) {
		slider.slick({
			slidesToShow: 2,
			slidesToScroll: 1,
			infinite: true,
			cssEase: 'ease-in-out',
			useTransform: true,
			speed: 750,
			prevArrow: '<button type="button" class="partners__prev"><svg class="icon icon-arrow-slider"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-slider"></use></svg></button>',
			nextArrow: '<button type="button" class="partners__next"><svg class="icon icon-arrow-slider"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-slider"></use></svg></button>'
		});
	}

	function initWhyAboutSlider(slider) {
		slider.slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			infinite: true,
			cssEase: 'ease-in-out',
			useTransform: true,
			speed: 750,
			prevArrow: '<button type="button" class="why__prev"><svg class="icon icon-arrow-slider"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-slider"></use></svg></button>',
			nextArrow: '<button type="button" class="why__next"><svg class="icon icon-arrow-slider"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-slider"></use></svg></button>',
			responsive: [
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1
					}
				}
			]
		});
	}
});

$(function () {
	enquire.register('screen and (min-width: 1220px)', {
		setup: function () {

		},
		match: function () {

		},
		unmatch: function () {

		}
	}, true);

	var History = {
		ind: false,
		// init
		init: function () {
			if (!this.ind) {
				this.cacheDom();
				this.setOnStart();
				this.resize();
			}
			this.ind = true;
		},
		//cacheDom
		cacheDom: function () {
			// defaults
			this.windowWidth = $(window).width();

			// nav
			this.$nav = $('.js-history-nav');
			this.$navEl = this.$nav.children();

			//for
			this.$for = $('.js-history-for');
			this.$forEl = this.$for.children();

			//line
			this.$line = $('.js-history-line');
			this.$lineContainer = this.$line.children('.container');
			this.$line2 = this.$line.children('.history__line2');
			this.$lineContainerDots = this.$line.find('.history__line-dots');

			//scrollMagic
			this.historyController = new ScrollMagic.Controller();
			this.tweenTimeline = new TimelineMax({
				onComplete: function () {
					History.initAfterTimeline();
				}
			});
			this.tweenTimelineTime = 0.75;
			this.navElTimeout = ($(window).width() - this.$lineContainer.width()) / $(window).width() / 2 * this.tweenTimelineTime;
			this.navElTime = (this.tweenTimelineTime - (2 * this.navElTimeout)) / (this.$navEl.length);
		},
		// indInit for first load
		indInit: false,
		//setOnStart
		setOnStart: function () {
			var _lineComplete;
			//active nav
			this.$navEl.eq(Math.floor(this.$navEl.length / 2)).addClass('is-active');
			//put dots
			for (var i = 0; i < this.$navEl.length; i++) {
				this.$lineContainerDots.append('<div class="history__line-dot"></div>');
			}
			//save dot [object]
			this.$dot = this.$lineContainerDots.children();

			//animateFirstLoad
			this.animateFirstLoad = function () {
				this.tweenTimeline
					.addLabel('start')
					.from(this.$line, this.tweenTimelineTime, {
						x: '-100%',
						ease: Linear.easeNone
					}, 'start')
					.staggerFromTo($('.history__line-dot'), 0.25, {
						alpha: 0
					}, {
						alpha: 1,
						ease: Power3.easeInOut
					}, this.navElTime / 2)
					.staggerFrom(this.$navEl, 0.25, {
						alpha: 0,
						ease: Power3.easeInOut
					}, this.navElTime, this.navElTimeout);

				this.historyController.scene = new ScrollMagic.Scene({
					triggerElement: '.js-history-nav',
					offset: $(window).height() / (-3)
				})
					.setTween(this.tweenTimeline)
					.addTo(this.historyController);

				this.historyController.scene.on('start', function () {
					this.remove();
				});
			};

			//drawLine2
			this.drawLine2 = function (reverse) {
				this.checkActiveIndex();

				var _currentDot = this.$dot.eq(this.activeIndex).prevAll().andSelf(),
					_nextDot = this.$dot.eq(this.activeIndex).nextAll().toArray().reverse(),
					_currentNav = this.$navEl.eq(this.activeIndex).prevAll().andSelf(),
					_nextNav = this.$navEl.eq(this.activeIndex).nextAll().toArray().reverse(),
					_activeDotOffset = this.$dot.eq(this.activeIndex).offset().left,
					_line2TabletIndent = $(window).width() < 1220 ? -23 : 0,
					_color = '#A70A3F',
					_color2 = '#D286A0',
					_line2Speed = reverse === 'reverse' ? 0.25 : 0.5,
					_dotTimeout = _line2Speed - (_activeDotOffset - (this.$dot.eq(0).offset().left)) / _activeDotOffset * _line2Speed;

				TweenMax.set(this.$line2, {
					alpha: 1
				});

				TweenMax.to(this.$line2, _line2Speed, {
					width: _activeDotOffset + _line2TabletIndent + 3,
					ease: Linear.easeNone,
					onComplete: function () {
						History.drawCompleteLine();
					}
				});

				TweenMax.staggerTo(_nextDot, 0.25, {
					'background-color': _color2,
					ease: Linear.easeNone,
					delay: 0
				}, ((_line2Speed - _dotTimeout) / (_nextDot.length - 1)));

				TweenMax.staggerTo(_currentDot, 0.25, {
					'background-color': _color,
					ease: Linear.easeNone,
					delay: _dotTimeout
				}, ((_line2Speed - _dotTimeout) / (_currentDot.length - 1)));

				TweenMax.staggerTo(_nextNav, 0.25, {
					'color': _color2,
					ease: Linear.easeNone,
					delay: 0
				}, ((_line2Speed - _dotTimeout) / (_nextNav.length - 1)));

				TweenMax.staggerTo(_currentNav, 0.25, {
					'color': '#A70A3F',
					ease: Linear.easeNone,
					delay: _dotTimeout
				}, ((_line2Speed - _dotTimeout) / (_currentDot.length - 1)));
			};

			//drawCompleteLine
			this.drawCompleteLine = function (reverse) {
				this.checkActiveIndex();

				var _height = reverse === 'reverse' ? 0 : 100,
					_speed = reverse === 'reverse' ? 0.25 : 0.75,
					_ease = reverse === 'reverse' ? Power1.easeIn : Power1.easeOut;

				if (reverse !== 'reverse') {
					this.$dot.eq(this.activeIndex).append('<div class="history__line-complete"></div>');
					_lineComplete = this.$lineContainerDots.find('.history__line-complete');
				}

				TweenMax.to(_lineComplete, _speed, {
					height: _height,
					ease: _ease,
					onComplete: function () {
						if (reverse !== 'reverse') {
							History.toggleTab();
						} else if (reverse === 'reverse') {
							$(this.target).remove();
							History.drawLine2('reverse');
						}
					}
				});
			};

			this.animateFirstLoad();
		},
		//initAfterTimeline
		initAfterTimeline: function () {
			this.drawLine2();
		},
		//checkActiveIndex
		checkActiveIndex: function () {
			this.activeIndex = this.$navEl.filter('.is-active').index();
		},
		// toggleTab
		toggleTab: function () {
			this.$forEl.removeClass('is-active');
			var _thisTab = this.$forEl.filter('[data-index="' + this.$navEl.filter('.is-active').attr('data-index') + '"]');

			if (((this.activeIndex == ((this.$navEl.length - 1) / 2)) || ((this.activeIndex - 0.5) == ((this.$navEl.length - 1) / 2)) || ((this.activeIndex + 0.5) == ((this.$navEl.length - 1) / 2))) && ($(window).width() >= 1220)) {
				_thisTab.css({
					'left': _thisTab.css('left', this.$dot.eq(this.activeIndex).offset().left - this.$dot.eq(0).offset().left - (_thisTab.outerWidth() / 2) + 12)
				});
			} else if (this.activeIndex <= (this.$navEl.length / 2)) {
				_thisTab.css('left', this.$dot.eq(this.activeIndex).offset().left - this.$dot.eq(0).offset().left + 10);
			} else {
				_thisTab.css('left', this.$dot.eq(this.activeIndex).offset().left - this.$dot.eq(0).offset().left - _thisTab.outerWidth() - 20);
			}

			_thisTab.addClass('is-active').fadeIn(500, function () {
				if (!History.indInit) {
					History.indInit = true;
					History.bindEvents();
				}
			});
		},
		// toggleNav
		toggleNav: function () {
			History.checkActiveIndex();
			changeActiveNav(this);
			hideCurrentForEl();

			function changeActiveNav(element) {
				History.$navEl.eq(History.activeIndex).removeClass('is-active');
				$(element).addClass('is-active');
			}

			function hideCurrentForEl() {
				History.$forEl.eq(History.activeIndex).fadeOut(250, function () {
					History.drawCompleteLine('reverse');
				});
			}
		},
		// bindEvents
		bindEvents: function () {
			this.$navEl.on('click', this.toggleNav);
		},
		resize: function () {
			$(window).resize(function () {
				if ($(window).width() != History.windowWidth) {
					History.$navEl.filter('.is-active').trigger('click');

					if ($(window).width() > 767) {
						History.$lineContainer.show();
						History.$navEl.off('click', History.toggleNav).on('click', History.toggleNav);
						History.$forEl.eq(History.activeIndex).css('opacity', '1').slideUp(250);
						History.$lineContainerDots.find('.history__line-complete').fadeOut(250, function () {
							$(this).remove();
						});
						History.$line2.css('width', 0);
						History.$navEl.css('color', '#D286A0');
						History.$dot.css('background-color', '#D286A0');
					} else {
						History.$navEl.off('click', History.toggleNav).css({'color': '', 'opacity': '1'});
						History.$forEl.css({'left': '', 'transform': 'none', 'display': 'block'});
					}

					History.windowWidth = $(window).width();
				}
			});
		}
	};

	if ($('.page-about').length) {
		if ($(window).width() > 767) {
			History.init();
		}
		$(window).resize(function () {
			if ($(window).width() > 767) {
				History.init();
			}
		});
	}
});