$(function() {
  var css = {
    active: 'active'
  };

  /** BIG TABS. MAIN SCENES CONTROL */
  (function() {
    var $bigTabs = $('.tab[data-tab-for]');
    var $bigTabsScreens = $('.tab__screen[data-tab-screen-id]');

    /**
     * Close all tabs.
     */
    function closeTabs() {
      // remove active state from all tabs
      $bigTabs.removeClass(css.active);

      // remove active state from all screens
      $bigTabsScreens.removeClass(css.active);
    }

    /**
     * Open selected tab.
     *
     * @param {jQuery} $targetTab
     */
    function openTab($targetTab) {
      var screenId = $targetTab.data('tab-for');
      var $targetScreen = $bigTabsScreens.filter('[data-tab-screen-id=' + screenId + ']');

      // add active state to the current tab
      $targetTab.addClass(css.active);

      // add active state to the related screen
      $targetScreen.addClass(css.active);
    }

    // add event listener on tabs
    $bigTabs.on('click tap', function() {
      var $this = $(this);

      closeTabs();
      openTab($this);
    });
  })();

  /** SMALL TABS. AJAX */
  (function() {
    var $smallTabs = $('.tab__screen_menu [data-screen-content-id]');
    var $smallTabsScreens = $('.content_tab[data-content-id]');

    var globalAnimationFlag = false;

    $smallTabs.on('click tap', function (e) {
      e.preventDefault();

      var $this = $(this);

      // do nothing when clicked on active tab
      if ($this.hasClass(css.active)) return;

      var dataContentTarget = $this.data('screen-content-id');
      var tabUrl = $this.data('tab-url');
      var tabId = $this.data('tab-id');
      var $thisScreen = $smallTabsScreens.filter('[data-content-id=' + dataContentTarget + ']');

      if (!globalAnimationFlag) {
        globalAnimationFlag = true;

        setActiveTab($this);
        changeTabContent($thisScreen, tabUrl, tabId);
      }
    });

    /**
     * Set active state to specified tab.
     *
     * @param {jQuery} $tab
     */
    function setActiveTab ($tab) {
      var $tabs = $tab.parents('.tab__screen_menu').find('li');

      // remove active state from other tabs
      $tabs.removeClass(css.active);
      // add active state to the clicked tab
      $tab.addClass(css.active);
    }

    /**
     * Change content in tab.
     *
     * @param {jQuery} $screen
     * @param {String} tabUrl
     * @param {Number} tabId
     */
    function changeTabContent($screen, tabUrl, tabId) {
      var elements = {
        $video: $screen.find('.content_tab__video'),
        $title: $screen.find('.content_tab__title'),
        $subtitle: $screen.find('.content_tab__subtitle'),
        $link: $screen.find('.content_tab__links'),
        $button: $screen.find('.content_tab__button')
      };

      TabContentChange.run(elements, tabUrl, tabId);
    }

    /** Animations/AJAX controller */
    var TabContentChange = (function() {
      var initialAnimationFlag = false;
      var ANIMATION_TIME = 0.4;
      var ANIMATION_TIME_TEXT = 0.3;
      var ANIMATION_EASING = Power1.easeInOut;

      /**
       * Run/reverse animation for specified elements.
       *
       * @param {Object} elements
       */
      function allAnimations(elements) {
        var $titleText = elements.$title.find('span');
        var $subtitleText = elements.$subtitle.find('span');
        var $linkLinks = elements.$link.find('a');
        var $animatedButton = elements.$button.find('.d_animation__button');
        var $animatedVideo = elements.$video.find('.d_animation__video');

        function run() {
          // animation started. set global flag to positive
          initialAnimationFlag = true;

          // title
          TweenMax.to($titleText, ANIMATION_TIME_TEXT,
            { css: { y: '-=100%', opacity: 0 }, ease: ANIMATION_EASING  }
          );
          // subtitle
          TweenMax.to($subtitleText, ANIMATION_TIME_TEXT,
            { css: { y: '-=50%', opacity: 0 }, ease: ANIMATION_EASING  }
          );
          // links
          TweenMax.fromTo($linkLinks, ANIMATION_TIME_TEXT,
            { css: { x: '0px', opacity: 1 } },
            { css: { x: '30px', opacity: 0 }, ease: ANIMATION_EASING }
          );
          // button
          TweenMax.fromTo($animatedButton, ANIMATION_TIME,
            { css: { left: 0, width: 0 } },
            { css: { width: '100%' }, ease: ANIMATION_EASING }
          );
          // video
          TweenMax.fromTo($animatedVideo, ANIMATION_TIME,
            { css: { top: 0, height: 0 } },
            { css: { height: '100%' },
              ease: ANIMATION_EASING,
              onComplete: function () {
                initialAnimationFlag = false;
              }
            }
          );
        }

        function reverse() {
          // title
          TweenMax.to($titleText, ANIMATION_TIME_TEXT,
            { css: { y: '0%', opacity: 1 }, ease: ANIMATION_EASING  }
          );
          // subtitle
          TweenMax.to($subtitleText, ANIMATION_TIME_TEXT,
            { css: { y: '0%', opacity: 1 }, ease: ANIMATION_EASING  }
          );
          // links
          TweenMax.fromTo($linkLinks, ANIMATION_TIME_TEXT,
            { css: { x: '30px', opacity: 0 } },
            { css: { x: '0px', opacity: 1 }, ease: ANIMATION_EASING  }
          );
          // button
          TweenMax.to($animatedButton, ANIMATION_TIME,
            { css: { left: '100%' }, ease: ANIMATION_EASING  }
          );
          // video
          TweenMax.to($animatedVideo, ANIMATION_TIME, {
            css: { top: '100%' },
            ease: ANIMATION_EASING,
            onComplete: function () {
              // reverse animation finished. set global animation flag to negative
              globalAnimationFlag = false;
            }
          });
        }

        return {
          run: run,
          reverse: reverse
        };
      }

      /**
       * Parse AJAX response. Change/append/replace data etc.
       *
       * @param {Object} remoteData
       */
      function parseResponse(remoteData) {
        if (remoteData.replaces instanceof Array) {
          for (var i = 0, ilen = remoteData.replaces.length; i < ilen; i++) {
            $(remoteData.replaces[i].what).replaceWith(remoteData.replaces[i].data);
          }
        }
        if (remoteData.append instanceof Array) {
          for (i = 0, ilen = remoteData.append.length; i < ilen; i++) {
            $(remoteData.append[i].what).append(remoteData.append[i].data);
          }
        }
        if (remoteData.content instanceof Array) {
          for (i = 0, ilen = remoteData.content.length; i < ilen; i++) {
            $(remoteData.content[i].what).html(remoteData.content[i].data);
          }
        }
        if (remoteData.js) {
          $("body").append(remoteData.js);
        }
        if (remoteData.refresh) {
          window.location.reload(true);
        }
        if (remoteData.redirect) {
          window.location.href = remoteData.redirect;
        }
      }

      /**
       * Wait until condition become 'false' and run function.
       *
       * @param {Function} condition - function that should return needed condition
       * @param {Number} waitMs - timeout between re-check
       * @param {Function} func - called when condition isn't 'false'
       * @param {Object} [context] - context passed to main function
       * @param {Array} [args] - arguments passed to main function
       */
      function waitAndRun(condition, waitMs, func, context, args) {
        if (condition.call(null, null)) {
          setTimeout(function() {
            waitAndRun(condition, waitMs, func);
          }, waitMs);
          return;
        }
        func.apply(context, args);
      }

      /**
       * Change tab.
       *
       * @param {Object} elements - elements to animate
       * @param {String} tabUrl - AJAX request url
       * @param {Number} tabId - ID of the clicked tab
       */
      function run(elements, tabUrl, tabId) {
        $.ajax({
          url: tabUrl,
          method: 'get',
          dataType: 'json',
          data: tabId,
          beforeSend: function () {
            allAnimations(elements).run();
          },
          success: function (response) {
            // wait for animation to finish
            waitAndRun(function () { return initialAnimationFlag; }, 50, function () {
              // parse response
              parseResponse(response);
              // run animation when video will load
              elements.$link.find('a').css('opacity', '0');
              elements.$video.find('video').get(0).addEventListener('loadeddata', function() {
                // reverse animation (reveal elements)
                allAnimations(elements).reverse();
              });
            });
          },
          error: function (xhr) {
            // wait for animation to finish
            waitAndRun(function() { return initialAnimationFlag; }, 50, function () {
              // console log error
              console.error('Error! ' + xhr.statusText);
              // reverse animation (reveal elements)
              allAnimations(elements).reverse();
              elements.loog
            });
          }
        });
      }

      return {
        run: run
      };
    }());
  })();
});